package com.mycompany.exercicio4g1;
import java.util.Scanner;

public class exercicio4g1 {

    public static void main(String[] args) {
        float N, num,conta, soma, somaneg,contaneg;
        float perc_positivos, media,medianeg ;
        
        Scanner ler = new Scanner (System.in);
        System.out.println("Quantos números quer ler:");
        conta=0;
        soma=0;
        somaneg=0;
        contaneg=0;
        do {
        N = ler.nextInt();
        } while (N<0);
        
        System.out.println("");
        
        for (int i=1; i<N+1; i++) {
            System.out.println("Insira um numero");
            num= ler.nextInt();
            soma=soma+num;
            if (num>0) {
               conta++;   
            }
            if (num<0) {
               contaneg++;  
               soma=soma+num;
            }
        }
        media=soma/N;  
        medianeg= somaneg/contaneg;
        perc_positivos=(conta/N)*100;  
        System.out.printf("A percentagem de positivos é: %.2f", perc_positivos);
        System.out.println("");
        System.out.printf("A media dos numeros lidos é: %.2f", media);
        System.out.println("");
        System.out.printf("A soma dos negativos é: %.2f", somaneg);
        System.out.println("");
        System.out.printf("A media dos numeros negativos lidos é: %.2f", medianeg);
        System.out.println("");
    }
}
